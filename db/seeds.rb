# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
    TipoPessoa.create :nm_tipopessoa => 'Cliente', :tp_pessoa => 'C'
    TipoPessoa.create :nm_tipopessoa => 'Vendedor', :tp_pessoa => 'V'
    TipoPessoa.create :nm_tipopessoa => 'Fornecedor', :tp_pessoa => 'F'
    TipoPessoa.create :nm_tipopessoa => 'Transportadora', :tp_pessoa => 'T'

    TipoMovimento.create :ds_movimento => 'Entrada'
    TipoMovimento.create :ds_movimento => 'Saída'

    TipoFormaPagamento.create :ds_pagamento => 'A vista'
    TipoFormaPagamento.create :ds_pagamento => 'A prazo'
    TipoFormaPagamento.create :ds_pagamento => 'Cartão de crédito'
    TipoFormaPagamento.create :ds_pagamento => 'Cheque'

    User.create :email => 'denysnando@gmail.com', :name => 'denys', :password => '123123123', :password_confirmation => '123123123'
    User.create :email => 'isaqueakamine@gmail.com', :name => 'isaque', :password => 'universal', :password_confirmation => 'universal'
    User.create :email => 'bruno_demello@hotmail.com', :name => 'Bruno', :password => 'telefone', :password_confirmation => 'telefone'
