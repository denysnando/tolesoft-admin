class AlterFormaPagamento < ActiveRecord::Migration
  def change
    remove_column :forma_pagamentos, :pedido_id
    remove_column :forma_pagamentos, :vl_pagamento
    remove_column :forma_pagamentos, :vl_entrada
    rename_column :forma_pagamentos, :dias_pagamento, :dia_pagamento
    rename_column :forma_pagamentos, :nr_pagamento, :qt_pagamento
    add_column :forma_pagamentos, :possui_entrada, :boolean
  end
end
