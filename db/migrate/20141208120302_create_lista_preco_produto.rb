class CreateListaPrecoProduto < ActiveRecord::Migration
  def change
    create_table :lista_preco_produtos do |t|
      t.integer :produto_id, :references => :produtos
      t.integer :lista_preco_id, :references => :lista_precos
      t.float :vl_produto
      t.timestamps
    end
  end
end
