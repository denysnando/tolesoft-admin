class AddColumnsToPessoas < ActiveRecord::Migration
  def change
    add_column :pessoas, :estado_id, :integer
    add_column :pessoas, :cidade_id, :integer
    add_column :pessoas, :tipo_pessoa_id, :integer
  end
end
