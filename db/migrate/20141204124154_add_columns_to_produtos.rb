class AddColumnsToProdutos < ActiveRecord::Migration
  def change
    add_column :produtos, :vl_precovenda, :float
    add_column :produtos, :qt_minima, :float
    add_column :produtos, :vl_lucroproduto, :float
    add_column :produtos, :vl_custo, :float
    add_column :produtos, :vl_customedio, :float
    add_column :produtos, :vl_comissaoproduto, :float
    add_column :produtos, :vl_operacional, :float
    add_column :produtos, :pro_localizacao, :string
    add_column :produtos, :pro_vendanegativa, :boolean
    add_column :produtos, :pro_semdevolucao, :boolean
    add_column :produtos, :pro_cancelado, :boolean
    add_column :produtos, :pro_aplicacao, :string
    add_column :produtos, :dt_ultimavenda, :date
    add_column :produtos, :dt_ultimacompra, :date
  end
end
