class AddFormaPagamentoToPedido < ActiveRecord::Migration
  def change
    add_column :pedidos, :forma_pagamento_id, :integer, references: :forma_pagamentos
  end
end
