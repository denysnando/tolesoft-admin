class CreateMovimentoEstoque < ActiveRecord::Migration
  def change
    create_table :movimento_estoques do |t|
      t.integer :tp_movimento_id, references: :tipo_movimento
      t.timestamps
    end
  end
end
