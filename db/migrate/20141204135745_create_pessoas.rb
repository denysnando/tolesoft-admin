class CreatePessoas < ActiveRecord::Migration
  def change
    create_table :pessoas do |t|
      t.string :nm_pessoa
      t.string :nm_fantasia
      t.string :nr_cpfcnpj
      t.string :nr_rginscricaomunicipal
      t.string :end_cep
      t.string :end_rua
      t.string :end_bairro
      t.string :end_complemento
      t.string :end_numero
      t.string :nr_fone
      t.string :nr_celular
      t.string :email_pessoanfe
      t.string :email_pessoa
      t.string :site_pessoa

      t.timestamps
    end
  end
end
