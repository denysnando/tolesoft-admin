class CreateFormaPagamentos < ActiveRecord::Migration
  def change
    create_table :forma_pagamentos do |t|
      t.integer  :pedido_id
      t.integer  :nr_pagamento
      t.float    :vl_pagamento
      t.float    :vl_entrada
      t.integer  :dias_pagamento
      t.timestamps
    end
  end
end
