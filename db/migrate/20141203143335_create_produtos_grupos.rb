class CreateProdutosGrupos < ActiveRecord::Migration
  def change
    create_table :produtos_grupos do |t|
      t.string :ds_grupo
      t.timestamps
    end
  end
end
