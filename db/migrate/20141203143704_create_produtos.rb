class CreateProdutos < ActiveRecord::Migration
  def change
    create_table :produtos do |t|
      t.string :ds_produto
      t.string :ean_produto
      t.integer :produtos_grupo_id, references: :produtos_grupos
      t.integer :produtos_unidade_medida_id, references: :produtos_unidade_medidas
      t.timestamps
    end
  end
end
