class CreateProdutosUnidadeMedida < ActiveRecord::Migration
  def change
    create_table :produtos_unidade_medidas do |t|
      t.string :cd_unidademedida
      t.string :ds_unidademedida
      t.timestamps
    end
  end
end
