class ChangeMovimentos < ActiveRecord::Migration
  def change
    drop_table :movimento_estoque_produtos

    add_column :movimento_estoques, :compra_produto_id, :integer
    add_column :movimento_estoques, :pedido_produto_id, :integer
  end
end
