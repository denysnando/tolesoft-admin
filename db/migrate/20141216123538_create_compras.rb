class CreateCompras < ActiveRecord::Migration
  def change
    create_table :compras do |t|
      t.integer :fornecedor_id, references: :pessoa
      t.float   :vl_total

      t.timestamps
    end
  end
end
