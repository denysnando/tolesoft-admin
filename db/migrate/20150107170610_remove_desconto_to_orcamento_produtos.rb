class RemoveDescontoToOrcamentoProdutos < ActiveRecord::Migration
  def change
    remove_column :orcamento_produtos, :desconto_produto
  end
end
