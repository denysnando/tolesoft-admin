class CreateEstados < ActiveRecord::Migration
  def change
    create_table :estados do |t|
      t.string :nm_estado

      t.timestamps
    end
  end
end
