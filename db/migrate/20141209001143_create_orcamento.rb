class CreateOrcamento < ActiveRecord::Migration
  def change
    create_table :orcamentos do |t|
      t.integer :cliente_id, references: :pessoa
      t.integer :vendedor_id, references: :pessoa
      t.float :vl_total

      t.timestamps
    end
  end
end
