class AddQtProdutoToMovimentoEstoques < ActiveRecord::Migration
  def change
    add_column :movimento_estoques, :qt_produto, :float,  default: 0
  end
end
