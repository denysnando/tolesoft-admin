class CreateOrcamentoFormaPagamento < ActiveRecord::Migration
  def change
    create_table :orcamento_forma_pagamentos do |t|
      t.integer :orcamento_id, references: :orcamentos
      t.integer :forma_pagamento_id, references: :forma_pagamentos
      t.timestamps
    end
  end
end
