class CreatePedido < ActiveRecord::Migration
  def change
    create_table :pedidos do |t|
      t.integer :cliente_id, references: :pessoa
      t.integer :vendedor_id, references: :pessoa
    end
  end
end
