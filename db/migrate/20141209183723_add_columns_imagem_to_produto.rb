class AddColumnsImagemToProduto < ActiveRecord::Migration
  def change
    add_column :produtos, :imagem_id, :integer, references: :imagems
  end
end
