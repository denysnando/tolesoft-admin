class CreateTipoPessoas < ActiveRecord::Migration
  def change
    create_table :tipo_pessoas do |t|
      t.string :tp_pessoa
      t.string :nm_tipopessoa

      t.timestamps
    end
  end
end
