class CreateComprasProdutos < ActiveRecord::Migration
  def change
    create_table :compra_produtos do |t|
      t.integer :produto_id, references: :produtos
      t.float :qt_produto
      t.float :sub_total
      t.integer :compra_id, references: :compras
    end
  end
end
