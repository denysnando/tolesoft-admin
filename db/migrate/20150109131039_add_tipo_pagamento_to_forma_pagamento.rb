class AddTipoPagamentoToFormaPagamento < ActiveRecord::Migration
  def change
    add_column :forma_pagamentos, :tipo_forma_pagamento_id, :integer, references: :tipo_forma_pagamentos
  end
end
