class AddAttachmentAssetToImagems < ActiveRecord::Migration
  def self.up
    change_table :imagems do |t|
      t.attachment :asset
    end
  end

  def self.down
    remove_attachment :imagems, :asset
  end
end
