class CreateCidades < ActiveRecord::Migration
  def change
    create_table :cidades do |t|
      t.string :nm_cidade
      t.integer :estado_id

      t.timestamps
    end
  end
end
