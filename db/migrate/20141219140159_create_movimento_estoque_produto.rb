class CreateMovimentoEstoqueProduto < ActiveRecord::Migration
  def change
    create_table :movimento_estoque_produtos do |t|
      t.integer :pedido_id, references: :pedido
      t.integer :compra_id, references: :compra
      t.integer :movimento_estoque_id, references: :movimento_estoque
    end
  end
end
