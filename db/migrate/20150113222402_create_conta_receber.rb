class CreateContaReceber < ActiveRecord::Migration
  def change
    create_table :conta_recebers do |t|
      t.integer :pedido_id, references: :pedidos
      t.float :vl_conta
      t.float :vl_pago
      t.date :dt_vencimento
      t.float :vl_juro
      t.timestamps
    end
  end
end
