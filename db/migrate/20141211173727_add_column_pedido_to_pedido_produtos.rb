class AddColumnPedidoToPedidoProdutos < ActiveRecord::Migration
  def change
    add_column :pedido_produtos, :pedido_id, :integer, reference: :pedidos
  end
end
