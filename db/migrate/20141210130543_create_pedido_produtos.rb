class CreatePedidoProdutos < ActiveRecord::Migration
  def change
    create_table :pedido_produtos do |t|
      t.integer :produto_id, references: :produtos
      t.float :qt_produto
      t.float :desconto_produto
      t.float :sub_total
    end
  end
end
