class Produto < ActiveRecord::Base
  belongs_to :produtos_grupo
  belongs_to :produtos_unidade_medida
  belongs_to :imagem

  has_many :lista_preco_produto
  has_many :orcamento_produtos
  has_many :pedido_produtos
  has_many :compra_produtos
  has_many :movimento_estoque_produto

  validates :ds_produto, presence: true
  validates :vl_precovenda, presence: true

  def self.finder(q)
    where("lower(ds_produto) like :q", q: "%#{q.downcase}%")
  end

  def name
    ds_produto
  end

  rails_admin do
    navigation_label 'Cadastros'
    list do
      field :id
      field :ds_produto
      field :produtos_grupo
      field :produtos_unidade_medida
      field :qt_estoque
      field :vl_precovenda
    end
    edit do
      field :ds_produto
      field :ean_produto
      field :produtos_grupo
      field :produtos_unidade_medida
      field :vl_precovenda
      field :qt_estoque
      field :qt_minima
      field :vl_lucroproduto
      field :vl_custo
      field :vl_customedio
      field :vl_comissaoproduto
      field :vl_operacional
      field :pro_localizacao
      field :pro_aplicacao
      field :dt_ultimavenda
      field :dt_ultimacompra
      field :pro_vendanegativa
      field :pro_semdevolucao
      field :pro_cancelado
      field :imagem
    end
  end

end
