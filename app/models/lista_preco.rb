class ListaPreco < ActiveRecord::Base
  has_many :lista_preco_produtos, :inverse_of => :lista_preco

  validates :nm_listapreco, presence: true


  accepts_nested_attributes_for :lista_preco_produtos, :allow_destroy => true

  def name
    nm_listapreco
  end

  rails_admin do
    navigation_label 'Cadastros'
    list do
      field :id
      field :nm_listapreco
    end
  end

end
