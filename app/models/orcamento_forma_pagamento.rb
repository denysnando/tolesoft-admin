class OrcamentoFormaPagamento < ActiveRecord::Base
  belongs_to :orcamento, :inverse_of => :orcamento_forma_pagamentos
  belongs_to :forma_pagamento

  rails_admin do
    visible false
  end
end
