class Estado < ActiveRecord::Base
  belongs_to :pessoa
  belongs_to :cidade

  def name
    nm_estado
  end

  rails_admin do
    visible false
  end

end
