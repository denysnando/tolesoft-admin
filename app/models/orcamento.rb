class Orcamento < ActiveRecord::Base
  belongs_to :cliente, class_name: 'Pessoa'
  belongs_to :vendedor, class_name: 'Pessoa'
  has_many :orcamento_produtos, :inverse_of => :orcamento
  has_many :produtos, :through => :orcamento_produtos
  has_many :orcamento_forma_pagamentos, :inverse_of => :orcamento

  accepts_nested_attributes_for :orcamento_produtos, :allow_destroy => true
  accepts_nested_attributes_for :orcamento_forma_pagamentos, :allow_destroy => true

  validates :cliente, presence: true
  validates :vendedor, presence: true
  validates :orcamento_produtos, presence: true


  before_save :update_total

  def to_s
    pessoa.nm_pessoa
  end

  rails_admin do
    navigation_label 'Financeiro'
    list do
      field :id
      field :cliente
      field :vendedor
      field :created_at
      field :vl_total
    end
    edit do
      field :cliente do
        associated_collection_cache_all false
        associated_collection_scope do
          orcamento = bindings[:object]
          Proc.new { |scope| scope = scope.where(tipo_pessoa_id: 1)
          }
        end
      end
      field :vendedor do
        associated_collection_cache_all false
        associated_collection_scope do
          orcamento = bindings[:object]
          Proc.new { |scope| scope = scope.where(tipo_pessoa_id: 2)
          }
        end
      end
      field :orcamento_produtos
      field :orcamento_forma_pagamentos
    end
  end

  private
  def update_total
    self.vl_total = orcamento_produtos.map(&:calc_sub_total).sum
  end
end
