class Imagem < ActiveRecord::Base
  belongs_to :produto
 has_attached_file :asset,
 :styles => {
  :thumb => "100x100#",
  :small  => "150x150>",
  :medium => "200x200" }
  validates_attachment_content_type :asset, :content_type => /\Aimage\/.*\Z/

  attr_accessor :delete_asset
  before_validation { self.asset.clear if self.delete_asset == '1' }

  def name
    ds_imagem
  end

rails_admin do
      navigation_label 'Cadastros'
      list do
        field :id
        field :ds_imagem
        field :asset
      end
      edit do
        field :ds_imagem
        field :asset
      end
  end

end
