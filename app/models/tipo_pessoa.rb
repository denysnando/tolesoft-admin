class TipoPessoa < ActiveRecord::Base
  belongs_to :pessoa

  def name
    nm_tipopessoa
  end

  rails_admin do
    visible false
  end

end
