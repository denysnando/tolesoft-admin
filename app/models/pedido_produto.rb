class PedidoProduto < ActiveRecord::Base
  belongs_to :pedido, :inverse_of => :pedido_produtos
  belongs_to :produto

  has_many :movimento_estoques, inverse_of: :pedido_produto

  validates :produto, presence: true
  validates :qt_produto, presence: true
  validates :sub_total, presence: true
  validate :check_estoque

  after_create :create_movimento!
  after_update :update_movimento!

  def check_estoque
    if qt_produto
      if produto.qt_estoque - self.qt_produto < 0 && !produto.pro_vendanegativa
        errors.add :qt_produto, "Estoque insuficiente"
        return false
      end
    end
  end

  def calc_sub_total
    qt_produto * sub_total
  end

  def create_movimento!
    MovimentoEstoque.create_saida!(self, self.qt_produto)
  end

  def update_movimento!
    if movimento_estoques.saidas.any?
      movimento = movimento_estoques.saidas.last
      if movimento.qt_produto != self.qt_produto
        MovimentoEstoque.create_entrada!(self)
        MovimentoEstoque.create_saida!(self, movimento.qt_produto)
      end
    end
  end

  def name
    pedido.id.to_s + " - " + produto.name
  end

  rails_admin do
    visible false
    edit do
     field :produto
     field :qt_produto
     field :sub_total
   end
 end
end
