class ListaPrecoProduto < ActiveRecord::Base
  belongs_to :produto
  belongs_to :lista_preco, :inverse_of => :lista_preco_produtos

  validates :valor, presence: true
  validates :produto_id, presence: true

  def name
    produto_id
  end

  rails_admin do
    visible false
  end

end
