class OrcamentoProduto < ActiveRecord::Base
  belongs_to :orcamento, :inverse_of => :orcamento_produtos
  belongs_to :produto

  validates :produto, presence: true
  validates :qt_produto, presence: true
  validates :sub_total, presence: true

  def to_s
    produto.ds_produto
  end

  def calc_sub_total
    qt_produto * sub_total
  end

  rails_admin do
    visible false
  end
end
