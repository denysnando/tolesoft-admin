class ProdutosUnidadeMedida < ActiveRecord::Base
  belongs_to :produtos

  validate :ds_unidademedida, :cd_unidademedida, presence: true

  def name
    ds_unidademedida
  end

  rails_admin do
    visible false
  end

end
