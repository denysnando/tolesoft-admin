class ProdutosGrupo < ActiveRecord::Base
  belongs_to :produto

  validate :ds_grupo, presence: true

  def name
    ds_grupo
  end

  rails_admin do
    visible false
  end

end
