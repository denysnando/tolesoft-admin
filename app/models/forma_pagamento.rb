class FormaPagamento < ActiveRecord::Base
  belongs_to :tipo_forma_pagamento, :inverse_of => :forma_pagamento
  belongs_to :pedido, :inverse_of => :forma_pagamento
  has_many :orcamento_forma_pagamentos

  def name
    ds_forma_pagamento
  end

  def self.finder(q)
    where("lower(ds_forma_pagamento) like :q", q: "%#{q.downcase}%")
  end

  rails_admin do
    navigation_label 'Cadastros'
    edit do
      field :tipo_forma_pagamento
      field :ds_forma_pagamento
      field :qt_pagamento
      field :vl_juro
      field :dia_pagamento
      field :possui_entrada
    end
  end
end
