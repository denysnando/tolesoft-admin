class TipoFormaPagamento < ActiveRecord::Base
  belongs_to :forma_pagamento, :inverse_of => :tipo_forma_pagamento
  def self.finder(q)
    where("lower(ds_pagamento) like :q", q: "%#{q.downcase}%")
  end

  def name
    ds_pagamento
  end

  rails_admin do
    visible false
  end
end
