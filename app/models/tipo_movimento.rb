class TipoMovimento < ActiveRecord::Base
  belongs_to :movimento_estoque

  def name
    ds_movimento
  end

rails_admin do
    visible false
  end

end
