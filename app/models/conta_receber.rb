class ContaReceber < ActiveRecord::Base
  belongs_to :pedido, :inverse_of => :conta_recebers


  rails_admin do
    navigation_label 'Financeiro'
    list do
      field :id
      field :pedido
      field :vl_conta
      field :vl_pago
      field :dt_vencimento
      field :created_at do
        date_format :default
      end
    end
  end
end
