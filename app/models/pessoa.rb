class Pessoa < ActiveRecord::Base
  belongs_to :estado
  belongs_to :cidade
  belongs_to :tipo_pessoa
  belongs_to :orcamento
  belongs_to :pedido

  validates :tipo_pessoa, presence: true

  scope :clientes, where(tipo_pessoa_id: 1)
  scope :vendedores, where(tipo_pessoa_id: 2)

  def self.finder(q, tipo)
    where("lower(nm_pessoa) like :q and tipo_pessoa_id = :tipo", q: "%#{q.downcase}%", tipo: tipo)
  end

  def name
    nm_pessoa
  end

  rails_admin do
    navigation_label 'Cadastros'
    list do
      field :nm_pessoa
      field :nm_fantasia
      field :nr_cpfcnpj
      field :estado
      field :cidade
    end
    edit do
      field :nm_pessoa
      field :nm_fantasia
      field :tipo_pessoa
      field :nr_cpfcnpj
      field :nr_rginscricaomunicipal
      field :estado
      field :cidade
      field :end_cep
      field :end_rua
      field :end_numero
      field :end_bairro
      field :end_complemento
      field :end_numero
      field :nr_fone
      field :nr_celular
      field :email_pessoanfe
      field :email_pessoa
      field :site_pessoa
    end
  end

end
