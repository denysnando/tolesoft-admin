class Pedido < ActiveRecord::Base
  belongs_to :cliente, class_name: 'Pessoa'
  belongs_to :vendedor, class_name: 'Pessoa'
  belongs_to :movimento_estoque_produto
  belongs_to :forma_pagamento, :inverse_of => :pedido

  has_many :conta_recebers, :inverse_of => :pedido
  has_many :pedido_produtos, :inverse_of => :pedido
  has_many :produtos, :through => :pedido_produtos

  accepts_nested_attributes_for :pedido_produtos, :allow_destroy => true
  accepts_nested_attributes_for :conta_recebers, :allow_destroy => true

  validates :cliente, presence: true
  validates :vendedor, presence: true

  before_save :update_total
  after_save :create_parcelas!

  def name
    id.to_s + " - " + cliente.nm_pessoa
  end

  private
  def update_total
    self.vl_total = pedido_produtos.map(&:calc_sub_total).sum
  end

  def create_parcelas!
    vl_parcela_pedido = (self.vl_total / self.forma_pagamento.qt_pagamento).round
    mes = 0
    for i in 1..self.forma_pagamento.qt_pagamento
      c = ContaReceber.new pedido_id: self.id, vl_conta: vl_parcela_pedido
      if i == 1
        c.dt_vencimento = Date.today
        if !self.forma_pagamento.possui_entrada
          c.dt_vencimento = c.dt_vencimento.change(day: self.forma_pagamento.dia_pagamento)
          c.dt_vencimento = c.dt_vencimento + (i).month
        end
      else
        c.dt_vencimento = Date.today
        c.dt_vencimento = c.dt_vencimento.change(day: self.forma_pagamento.dia_pagamento)
        if self.forma_pagamento.possui_entrada
          c.dt_vencimento = c.dt_vencimento + (i-1).month
        else
          c.dt_vencimento = c.dt_vencimento + (i).month
        end
      end
      c.save
    end
  end

  rails_admin do
    navigation_label 'Financeiro'
    list do
      field :id
      field :cliente
      field :vendedor
      field :created_at
      field :vl_total
    end
    edit do
      field :cliente do
        associated_collection_cache_all false
        associated_collection_scope do
          pedido = bindings[:object]
          Proc.new { |scope| scope = scope.where(tipo_pessoa_id: 1)
          }
        end
      end
      field :vendedor do
        associated_collection_cache_all false
        associated_collection_scope do
          pedido = bindings[:object]
          Proc.new { |scope| scope = scope.where(tipo_pessoa_id: 2)
          }
        end
      end
      field :pedido_produtos
      field :forma_pagamento
      field :conta_recebers
    end
  end
end
