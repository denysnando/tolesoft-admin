class Compra < ActiveRecord::Base
  belongs_to :fornecedor, class_name: 'Pessoa'
  belongs_to :movimento_estoque_produto
  has_many :compra_produtos, :inverse_of => :compra
  has_many :produtos, :through => :compra_produtos

  accepts_nested_attributes_for :compra_produtos, :allow_destroy => true

  validates :fornecedor, presence: true
  validates :compra_produtos, presence: true

  before_save :update_total

rails_admin do
      navigation_label 'Financeiro'
      list do
        field :id
        field :fornecedor
        field :created_at
        field :vl_total
      end
      edit do
        field :fornecedor do
          associated_collection_cache_all false
          associated_collection_scope do
            compra = bindings[:object]
            Proc.new { |scope| scope = scope.where(tipo_pessoa_id: 3)
            }
          end
        end
        field :compra_produtos
      end
  end

  private
  def update_total
    self.vl_total = compra_produtos.map(&:calc_sub_total).sum
  end
end
