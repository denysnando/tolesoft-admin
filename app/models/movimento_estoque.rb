class MovimentoEstoque < ActiveRecord::Base
  belongs_to :tp_movimento, class_name: 'TipoMovimento'
  belongs_to :compra_produto, inverse_of: :movimento_estoques
  belongs_to :pedido_produto, inverse_of: :movimento_estoques

  scope :entradas, -> { where(tp_movimento_id: 1) }
  scope :saidas,   -> { where(tp_movimento_id: 2) }

  def self.create_entrada!(obj)
    entrada = self.new(tp_movimento_id: 1, qt_produto: obj.qt_produto)
    if obj.is_a?(CompraProduto)
      entrada.compra_produto = obj
    else
      entrada.pedido_produto = obj
    end
    entrada.save

    produto = obj.produto
    produto.qt_estoque += entrada.qt_produto
    produto.save
  end

  def self.create_saida!(obj, qtde)
    qtde ||= obj.qt_produto

    saida = self.new(tp_movimento_id: 2, qt_produto: qtde)
    if obj.is_a?(CompraProduto)
      saida.compra_produto = obj
    else
      saida.pedido_produto = obj
    end
    saida.save

    produto = obj.produto
    produto.qt_estoque -= saida.qt_produto
    produto.save
  end

  rails_admin do
    navigation_label 'Financeiro'
    list do
      field :id
      field :tp_movimento
      field :pedido_produto
      field :compra_produto
      field :qt_produto
      field :created_at do
        date_format :default
      end
    end
  end

end
