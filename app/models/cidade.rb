class Cidade < ActiveRecord::Base
  belongs_to :estado
  belongs_to :pessoa

  def name
    nm_cidade
  end

  rails_admin do
    visible false
  end

end
