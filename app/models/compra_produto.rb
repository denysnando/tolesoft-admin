class CompraProduto < ActiveRecord::Base
  belongs_to :compra, :inverse_of => :compra_produtos
  belongs_to :produto

  has_many :movimento_estoques, inverse_of: :compra_produto

  validates :produto, presence: true
  validates :qt_produto, presence: true
  validates :sub_total, presence: true

  after_create :create_movimento!
  after_update :update_movimento!

  validates_numericality_of :qt_produto, presence: true
  validates_numericality_of :sub_total, presence: true

  def calc_sub_total
    qt_produto * sub_total
  end

  def create_movimento!
    MovimentoEstoque.create_entrada!(self)
  end

  def update_movimento!
    if movimento_estoques.entradas.any?
      movimento = movimento_estoques.entradas.last
      if movimento.qt_produto != self.qt_produto
        MovimentoEstoque.create_saida!(self, movimento.qt_produto)
        MovimentoEstoque.create_entrada!(self)
      end
    end
  end

  def name
    compra.id.to_s + " - " + produto.name
  end

  rails_admin do
    visible false
    edit do
     field :produto
     field :qt_produto
     field :sub_total
   end
 end

end
