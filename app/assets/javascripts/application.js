// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require cocoon
//= require_tree .


// $(document).ready(function(){
//   $('.dropdown').hover(function() {
//    $(this).toggleClass('open');
//  });

//   $('.selectpicker').selectpicker({
//     style: 'btn-info',
//     size: 4
//   });
// });


//$(document).on('nested:fieldAdded', 'form', function(content) {
//alert('asdf');
//});

function loadSelect2(element) {
  var url     = element.attr('data-source');
  var init_id = element.attr('data-init-id');
  var init_ds = element.attr('data-init-ds');
  if (element.val().length <= 0) {
    element.select2({
      ajax: {
        dataType: 'json',
        url: url,
        data: function (term, page) {
          return { q: term };
        },
        results: function(data, page) {
          return { results: data.results };
        }
      },
      initSelection: function (element, callback) {
        if (init_id != 'null' && init_ds != 'null' && element.val().length <= 0) {
          return callback({id: init_id, text: init_ds });
        }
      }
    }
    );
  }
}

$(document).ready(function() {
  loadMasks();
});

function loadMasks() {
  $('input.currency').mask("#.##0,00", {reverse: true});
  $('input.currency').css('text-align', 'right');
}
