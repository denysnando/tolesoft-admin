class ListaPrecoProdutosController < ApplicationController
  before_action :authenticate_user!

  private
  def lista_produto_params
    params.require(:lista_preco_produtos).permit(:vl_produto, :produto_id, :lista_preco_id)
  end
end
