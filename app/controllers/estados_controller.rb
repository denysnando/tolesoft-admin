class EstadosController < ApplicationController
  before_action :authenticate_user!

  private
  def estados_params
    params.require(:estados).permit(:nm_estado)
  end
end
