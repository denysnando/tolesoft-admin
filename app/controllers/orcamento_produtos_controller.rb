class OrcamentoProdutosController < ApplicationController
  before_action :authenticate_user!

  def index
    @orcamentoProdutos = OrcamentoProduto.all
  end

  def show
    @orcamentoproduto = Orcamento.find[:id]
  end

  def new
    @orcamentoproduto = OrcamentoProduto.new
  end

  def create
  end


  private
  def orcamento_produtos_params
    params.require(:orcamento_produto).permit(:orcamento_id, :produto_id, :qt_produto)
  end
end

