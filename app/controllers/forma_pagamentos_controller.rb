class FormaPagamentosController < ApplicationController
  before_action :authenticate_user!

  def index
    @forma_pagamentos = FormaPagamento.finder(params[:q]).page(params[:page]).per(params[:per])

  # also add the total count to enable infinite scrolling
  forma_pagamentos_count = FormaPagamento.finder(params[:q]).count

  respond_to do |format|
    format.json { render json: { total: forma_pagamentos_count, results: @forma_pagamentos.map { |e| {id: e.id, text: e.ds_forma_pagamento} } } }
  end
end

private
def forma_pagamentos_params
  params.require(:forma_pagamento).permit(:qt_parcela, :dia_pagamento, :tipo_forma_pagamentos_id,
   :possui_entrada, :ds_forma_pagamento)
end
end
