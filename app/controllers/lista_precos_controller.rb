class ListaPrecosController < ApplicationController
  before_action :authenticate_user!

  private
  def lista_params
    params.require(:lista_precos).permit(:nm_listapreco)
  end
end
