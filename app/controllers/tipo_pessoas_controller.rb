class TipoPessoasController < ApplicationController
  before_action :authenticate_user!

  private
  def tipo_pessoas_params
    params.require(:tipo_pessoas).permit(:tp_pessoa, :nm_tipopessoa)
  end
end
