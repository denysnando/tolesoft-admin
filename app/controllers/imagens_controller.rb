class ImagemsController < ApplicationController
  before_action :authenticate_user!

  private
  def imagens_params
    params.require(:imagems).permit(:fl_imagem_file_name, :fl_imagem_content_type,
                                    :fl_imagem_file_size, :fl_imagem_updated_at,
                                    :ds_imagem)
  end
end
