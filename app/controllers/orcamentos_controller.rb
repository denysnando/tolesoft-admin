class OrcamentosController < ApplicationController
  before_action :authenticate_user!

  def index
    @orcamentos = Orcamento.all
  end

  def show
    @orcamento = Orcamento.find(params[:id])
  end

  def new
    @orcamento = Orcamento.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @orcamento }
    end
  end

  def create
    @orcamento = Orcamento.new(orcamento_params)
    respond_to do |format|
      if @orcamento.save
        format.html { redirect_to @orcamento, notice: 'Orçamento Criado com Sucesso.' }
        format.json { render json: @orcamento, status: :created, location: @orcamento }
      else
        format.html { render action: "new" }
        format.json { render json: @orcamento.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  def orcamento_params
    params.require(:orcamento).permit(:cliente_id, :vendedor_id, :vl_total,
                                      orcamento_produtos_attributes:
                                      [:id, :produto_id, :qt_produto, :sub_total, :_destroy])
  end
end
