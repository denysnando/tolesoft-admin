class ProdutosUnidadeMedidasController < ApplicationController
  before_action :authenticate_user!

  private
  def unidade_medida_params
    params.require(:produtos_unidade_medidas).permit(:cd_unidademedida, :ds_unidademedida)
  end
end
