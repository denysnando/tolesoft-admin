class CompraProdutosController < ApplicationController
  before_action :authenticate_user!

  private
  def compra_produtos_params
    params.require(:compra_produto).permit(:compra_id, :produto_id, :qt_produto,
                                           :sub_total)
  end
end

