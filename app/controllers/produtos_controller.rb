class ProdutosController < ApplicationController
  before_action :authenticate_user!

  def index
    @produtos = Produto.finder(params[:q]).page(params[:page]).per(params[:per])

    # also add the total count to enable infinite scrolling
    produtos_count = Produto.finder(params[:q]).count

    respond_to do |format|
      format.json { render json: { total: produtos_count, results: @produtos.map { |e| {id: e.id, text: e.ds_produto} } } }
    end
  end
end
