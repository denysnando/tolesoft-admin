class CidadesController < ApplicationController
  before_action :authenticate_user!

  private
  def cidades_params
    params.require(:cidades).permit(:nm_cidade, :estado_id)
  end
end
