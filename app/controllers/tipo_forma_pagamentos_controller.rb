class TipoFormaPagamentosController < ApplicationController
  before_action :authenticate_user!

  def index
    @tipo_forma_pagamentos = TipoFormaPagamento.finder(params[:q]).page(params[:page]).per(params[:per])

    # also add the total count to enable infinite scrolling
    tipo_forma_pagamentos_count = TipoFormaPagamento.finder(params[:q]).count

    respond_to do |format|
      format.json { render json: { total: tipo_forma_pagamentos_count, results: @tipo_forma_pagamentos.map { |e| {id: e.id, text: e.ds_pagamento} } } }
    end
  end

  private
  def tipo_forma_pagamentos_params
    params.require(:tipo_forma_pagamento).permit(:ds_pagamento)
  end
end
