class PedidosController < ApplicationController
  before_action :authenticate_user!
  def index
    @pedidos = Pedido.all
  end

  def show
    @pedido = Pedido.find(params[:id])
  end

  def new
    @pedido = Pedido.new(forma_pagamento: FormaPagamento.new)
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @pedido }
    end
  end

  def edit
    @pedido = Pedido.find(params[:id])
  end

  def create
    @pedido = Pedido.new(pedido_params)
    respond_to do |format|
      if @pedido.save
        format.html { redirect_to @pedido, notice: 'Shopping list was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  private
  def pedido_params
    params.require(:pedido).permit(:cliente_id, :vendedor_id, :vl_total,
      pedido_produtos_attributes:
      [:id, :produto_id, :qt_produto, :sub_total, :_destroy])
  end
end


