class PessoasController < ApplicationController
  def index
    @pessoas = Pessoa.finder(params[:q], params[:tipo].to_i).page(params[:page]).per(params[:per])

    # also add the total count to enable infinite scrolling
    pessoas_count = Pessoa.finder(params[:q], params[:tipo].to_i).count

    respond_to do |format|
      format.json { render json: { total: pessoas_count, results: @pessoas.map { |e| {id: e.id, text: e.nm_pessoa} } } }
    end
  end

  private

  def pessoas_params
    params.require(:pessoas).permit(:nm_pessoa, :nm_fantasia, :site_pessoa, :nr_cpfcnpj,
                                    :nr_rginscricaomunicipal, :end_cep, :end_rua,
                                    :end_bairro, :end_complemento, :end_numero,
                                    :nr_fone, :nr_celular, :email_pessoa, :email_pessoanfe,
                                    :estado_id, :tipo_pessoa_id)
  end
end
