class OrcamentoProdutosController < ApplicationController
  before_action :authenticate_user!

  def index
    @pedidoprodutos = PedidoProduto.all
  end

  def show
    @pedidoproduto = PedidoProduto.find[:id]
  end

  private
  def pedido_produtos_params
    params.require(:pedido_produto).permit(:pedido_id, :produto_id, :qt_produto, :sub_total)
  end
end

