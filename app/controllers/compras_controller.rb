class ComprasController < ApplicationController
  before_action :authenticate_user!

  private
  def compra_params
    params.require(:compra).permit(:fornecedor_id, :vl_total)
  end
end
