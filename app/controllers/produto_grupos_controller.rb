class ProdutosGruposController < ApplicationController
  before_action :authenticate_user!

  private
  def produtos_grupos_params
    params.require(:produtos_grupos).permit(:ds_grupo)
  end
end
