RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  config.authenticate_with do
   warden.authenticate! scope: :user
 end
 config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new do
      except ['MovimentoEstoque']
    end
    export
    bulk_delete
    show do
      except ['MovimentoEstoque']
    end
    edit do
      except ['MovimentoEstoque']
    end
    delete do
      except ['MovimentoEstoque']
    end
    show_in_app
    ## With an audit adapter, you can add:
    #history_index
    #history_show
  end

  RailsAdmin.config do |config|
    config.model 'User' do
      navigation_label 'Cadastros'
      list do
        field :id
        field :name
        field :email
        field :last_sign_in_at
      end
      edit do
        field :name
        field :email
        field :password
        field :password_confirmation
      end
    end
  end
  #icones do menu de navegação
  #config.model 'User' do
  #  navigation_icon 'icon-user'
  #end
end
